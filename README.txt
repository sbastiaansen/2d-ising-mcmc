### Code used for TUE Project for the course Computational and Mathematical Physics ### 

### Topic: Markov Chain Monte Carlo (MCMC) of the 2D ferromagnetic Ising model with Metropolis Hastings steps ###


Code is labelled per exercise and has docstring where necessary, and is fully parallised for optimal performance. 
